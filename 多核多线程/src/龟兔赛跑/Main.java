package 龟兔赛跑;

import java.util.Scanner;

public class Main {
	
	
	static Scanner input = new Scanner(System.in);
	static int len;
	static int torspeed = 1,rubspeed = 2,torrest,rubrest,torlen = 0,rublen = 0;

	public static void main(String[] args) {	
		match();
	}
	
	
	public static void match(){
		System.out.println("请输入比赛长度:");
		len = input.nextInt();
		Tortoist tor = new Tortoist();
		tor.start();
		Rubbit rub = new Rubbit();
		rub.start();
	}
	
	
	
	//自定义乌龟线程内部类
	static class Tortoist extends Thread{
		private boolean stop = true;
		public void stopThread(){
			stop = false;
		}
		@Override
		public void run() {
			
			while(stop){
				if(rublen >= len){
					System.out.println("乌龟:我输了!");
					stop = false;
				}
				torlen += torspeed;	
				if(torlen < len){
					System.out.println("乌龟跑了"+ torlen + "米了");
				}else if(torlen >= len){
					System.out.println("乌龟跑了"+ len + "米了");
				}
				
				if(torlen == len){
					System.out.println("乌龟:我跑完了");
					stop = false;
				}
				
				
			}
			
		}
	}
	
	
	//自定义兔子线程内部类
	static class Rubbit extends Thread{
		private boolean stop = true;
		public void stopThread(){
			stop = false;
		}
		@Override
		public void run() {
			
			while(stop){
				if(torlen == len){
					System.out.println("兔子:我输了!");
					stop = false;
				}
				rublen += rubspeed;	
				if(rublen < len){
					System.out.println("兔子跑了"+ torlen + "米了");
				}else if(rublen >= len){
					System.out.println("兔子跑了"+ len + "米了");
				}
				
				if(rublen >= len){
					System.out.println("兔子:我跑完了");
					stop = false;
				}
				
				
			}
			
		}
	}
}