
create database 图书管理系统
on 
(name=product_data,
 filename='d:\图书管理系统_data.mdf')
log on
( name=product_log,
 filename='d:\图书管理系统_log.ldf')

use 图书管理系统
create table ReaderTypeItem
(
	typename varchar(100)not null primary key,
	maxBorrowcounts int,
)
create table ReaderItem
(
	name varchar(100),
	sex char(2) check(sex='男'or sex='女'),
	major varchar(100),
	id varchar(100),
	borrowid varchar(100),
	tel varchar(100),
	idtype varchar(100),
	idtime varchar(100),
	typename varchar(100),
	primary key(borrowid),
	foreign key (typename)references ReaderTypeItem(typename),
)
create table BooksTypeItem
(
	typename varchar(50)not null primary key,
	borrowdays int,
)
create table BooksItem
(
	name varchar(100),
	number varchar(50),
	bookid varchar(50),
	author varchar(50),
	publishing varchar(100),
	price char(20),
	counts int,
	typename varchar(50),
	primary key(bookid),
	foreign key (typename) references BooksTypeItem (typename)
)
create table BorrowItem
(
	borrowid varchar(100),
	bookid varchar(50),
	borrowtime varchar(50),
	returntime varchar(50),
	primary key (borrowid,bookid),
	foreign key (borrowid)references ReaderItem(borrowid),
	foreign key (bookid)references BooksItem(bookid)
)


create table AdministratorItem
(
	adminno varchar(50)not null primary key,
	name varchar(50),
	passwordid varchar(50)
)
create table OrderBooksItem
(
	name varchar(100),
	number varchar(50),
	bookid varchar(50),
	author varchar(50),
	publishing varchar(100),
	price char(20),
	intime varchar(20),
	ordercounts int,
	adminno varchar(50)references AdministratorItem(adminno),
	checkyn char(2) check (checkyn='是' or checkyn='否'),
	typename varchar(100),
	primary key (bookid) ,
	
)
insert into ReaderTypeItem
values('学生','60')
insert into ReaderTypeItem
values('教师','60')
insert into ReaderTypeItem
values('上班族','40')


insert into ReaderItem
values('张明','男','计算机科学与技术','2015714940301','15003741','15309806574','学生证','2015-09-15','学生')
insert into ReaderItem
values('王广','男','计算机科学与技术','2015714940302','15003742','15309806575','学生证','2015-09-15','学生')
insert into ReaderItem
values('韩秋','男','英语','20086180042000775','37003631','18713298976','教师证','2009-06-18','教师')
insert into ReaderItem
values('张海','男','政治','20086180042000430','37008971','18713298876','教师证','2009-06-18','教师')
insert into ReaderItem
values('张恒','男','数学','20086180042000653','37008972','18713298990','教师证','2009-06-18','教师')
insert into ReaderItem
values('江丽','女','Java工程师','131023199009081324','26006201','15901650001','身份证','2003-09-07','上班族')
insert into ReaderItem
values('郑达','男','平面设计师','131023198305081302','26006202','15901650002','身份证','2003-09-07','上班族')
insert into ReaderItem
values('应援','男','建筑工程师','131023198709091434','26006203','15901650003','身份证','2003-09-07','上班族')
insert into ReaderItem
values('古江','男','演员','131023199205101304','26006204','15901650004','身份证','2003-09-07','上班族')


insert into BooksTypeItem
values('文学类','10')
insert into BooksTypeItem
values('文教类','6')
insert into BooksTypeItem
values('艺术类','6')


insert into BooksItem
values('活着','100101','9787506365437','余华','作家出版社','20.0','50','文学类')
insert into BooksItem
values('平凡的世界','100104','9787530212004','路遥','北京十月文艺出版社','52.0','50','文学类')
insert into BooksItem
values('艺术发展史','500501','7530500651','贡布里希','天津人民美术出版社','6.9','50','艺术类')
insert into BooksItem
values('20世纪的西方美学','500502','9787307068735','张贤根','武汉大学出版社','34.0','50','艺术类')
insert into BooksItem
values('Java开发实战经典','300304','9787302202615','李兴华','清华大学出版社','79.8','50','文教类')
insert into BooksItem
values('数据库应用技术','300302','9787563523672','孙晨霞','北京邮电大学出版社','26','50','文教类')


insert into BorrowItem
values('26006203','9787506365437','2016-01-01','2016-12-01')


insert into AdministratorItem
values('00101','张志明','123456')
insert into AdministratorItem
values('00102','王金瑞','123456')
insert into AdministratorItem
values('00103','李青青','123456')


insert into OrderBooksItem
values('活着','100101','9787506365437','余华','作家出版社','20.0','2014-01-01','50','00101','是','文学类')
insert into OrderBooksItem
values('艺术发展史','500501','7530500651','贡布里希','天津人民美术出版社','6.9','2015-01-02','50','00102','是','艺术类')
