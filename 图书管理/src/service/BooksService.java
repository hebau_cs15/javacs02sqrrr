package service;

import java.util.ArrayList;

import vo.BooksItem;

import dao.BooksDao;
/**
 * 管理员服务类
 */
public class BooksService {
	private BooksDao booksDao = new BooksDao();
	//查询服务
	public ArrayList<BooksItem> queryBooksItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<BooksItem> data = booksDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addBooksItem(String name,String number,String bookid,String author,String publishing,String price,String counts,String bookstype){
		BooksItem thisBooks = new BooksItem(name,number,bookid,author,publishing,Double.parseDouble(price),Integer.parseInt(counts),bookstype);
		if(booksDao.addBooksItem(thisBooks)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updateBooksItem(String name,String number,String bookid,String author,String publishing,String price,String counts,String bookstype){
		ArrayList<BooksItem> data = queryBooksItem();
		for(int i = 0; i < data.size();i++){
			BooksItem fruit = data.get(i);
			if(fruit.getNumber().equals(number)){
				boolean delSuccess = booksDao.delBooksItem(number);
				BooksItem thisBooks = new BooksItem(name,number,bookid,author,publishing,Double.parseDouble(price),Integer.parseInt(counts),bookstype);
				boolean addSuccess = booksDao.addBooksItem(thisBooks);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delBooksItem(String number){
		ArrayList<BooksItem> data = queryBooksItem();
		for(int i = 0; i < data.size();i++){
			BooksItem books = data.get(i);
			if(books.getNumber().equals(number)){
				if(booksDao.delBooksItem(number)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
	//搜索服务
			public String[][] searchBookItem(String str,String number,String type) {
				ArrayList<BooksItem> data = queryBooksItem();
				ArrayList<BooksItem> list= new ArrayList<BooksItem>();
				for(int i = 0; i < data.size();i++){
					BooksItem book = data.get(i);
					if(book.getName().equals(str)){
						list.add(book);
					}else if(book.getNumber().equals(number)){
						list.add(book);
					}else if(book.getTypename().equals(type)){
						list.add(book);
					}
				}
				if(!list.equals(null)){
					String[][] tbody = listToArray(list);
					return tbody;
				}else{
					return null;
				}
				
			}
			//集合数组转为二维数组
		  	private String[][] listToArray(ArrayList<BooksItem> list) {
		  		String[][] tbody = new String[list.size()][10];
		  		for(int i = 0; i < list.size();i++){
		  			BooksItem reader = list.get(i);
		  			tbody[i][0] = reader.getName();
		  			tbody[i][1] = reader.getNumber();
		  			tbody[i][2] = reader.getBookid();
		  			tbody[i][3] = reader.getAuthor();
		  			tbody[i][4] = reader.getPublishing();
		  			tbody[i][5] = reader.getPrice()+"";
		  			tbody[i][6] = reader.getCounts()+"";
		  			tbody[i][7] = reader.getTypename();
		  		}
		  		return tbody;
		  	}
}
