package service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import vo.BorrowItem;
import dao.BorrowDao;
/**
 * 管理员服务类
 */
public class BorrowService {
	SimpleDateFormat   formatter  = new   SimpleDateFormat( "yyyy-MM-dd ");
	private BorrowDao borrowDao = new BorrowDao();
	//查询服务
	public ArrayList<BorrowItem> queryBorrowItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<BorrowItem> data = borrowDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addBorItem(String borrowid,String number,String borrowtime,String returntime){
		BorrowItem thisBor = new BorrowItem(borrowid,number,borrowtime=formatter.format(new Date()),formatter.format(new Date()));
		if(borrowDao.addBorrowItem(thisBor)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updateBorItem(String borrowid,String number,String borrowtime,String returntime){
		ArrayList<BorrowItem> data = queryBorrowItem();
		for(int i = 0; i < data.size();i++){
			BorrowItem bor = data.get(i);
			if(bor.getBookid().equals(number)){
				boolean delSuccess = borrowDao.delBorrowItem(number);
				BorrowItem thisBor = new BorrowItem(borrowid,number,borrowtime=formatter.format(new Date()),formatter.format(new Date()));
				boolean addSuccess = borrowDao.addBorrowItem(thisBor);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delBorrowItem(String number){
		ArrayList<BorrowItem> data = queryBorrowItem();
		for(int i = 0; i < data.size();i++){
			BorrowItem bor = data.get(i);
			if(bor.getBookid().equals(number)){
				if(borrowDao.delBorrowItem(number)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
}
