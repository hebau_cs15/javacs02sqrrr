package service;

import java.util.ArrayList;
import vo.InItem;
import dao.InDao;
/**
 * 管理员服务类
 */
public class InService {
	private InDao inDao = new InDao();
	//查询服务
	public ArrayList<InItem> queryInItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<InItem> data = inDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean adInItem(String intime,String number){
		InItem thisrt = new InItem(intime,number);
		if(inDao.addInItem(thisrt)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	/*public boolean updateInItem(String intime,String number){
		ArrayList<InItem> data = queryInItem();
		for(int i = 0; i < data.size();i++){
			InItem rt = data.get(i);
			if(rt.getIntime().equals(intime)){
				boolean delSuccess = inDao.delInItem(number);
				InItem thisrt = new InItem(typename,Integer.parseInt(maxBorrowcounts));
				boolean addSuccess = rtDao.addReadertypeItem(thisrt);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}*/
	//删除服务
	/*public int delInItem(String typename){
		ArrayList<ReaderTypeItem> data = queryReadertypeItem();
		for(int i = 0; i < data.size();i++){
			ReaderTypeItem rt = data.get(i);
			if(rt.getTypename().equals(typename)){
				if(rtDao.delReadertypeItem(typename)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}*/
}
