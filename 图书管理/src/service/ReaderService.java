package service;

import java.util.ArrayList;

import vo.ReaderItem;
import dao.ReaderDao;
/**
 * 管理员服务类
 */
public class ReaderService {
	private ReaderDao readerDao = new ReaderDao();
	//查询服务
	public ArrayList<ReaderItem> queryReaderItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<ReaderItem> data = readerDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addReaderItem(String name,String sex,String major,String id,String borrowid,String tel,String idtype,String idtime,String typename){
		ReaderItem thisReader = new ReaderItem(name,sex,major,id,borrowid,tel,idtype,idtime,typename);
		if(readerDao.addReaderItem(thisReader)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updateReaderItem(String name,String sex,String major,String id,String borrowid,String tel,String idtype,String idtime,String typename){
		ArrayList<ReaderItem> data = queryReaderItem();
		for(int i = 0; i < data.size();i++){
			ReaderItem reader = data.get(i);
			if(reader.getBorrowid().equals(borrowid)){
				boolean delSuccess = readerDao.delReaderItem(borrowid);
				ReaderItem thisReader = new ReaderItem(name,sex,major,id,borrowid,tel,idtype, idtime,typename);
				boolean addSuccess = readerDao.addReaderItem(thisReader);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delReaderItem(String borrowid){
		ArrayList<ReaderItem> data = queryReaderItem();
		for(int i = 0; i < data.size();i++){
			ReaderItem reader = data.get(i);
			if(reader.getBorrowid().equals(borrowid)){
				if(readerDao.delReaderItem(borrowid)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
	//搜索服务
		public String[][] searchReaderItem(String str,String a) {
			ArrayList<ReaderItem> data = queryReaderItem();
			ArrayList<ReaderItem> list= new ArrayList<ReaderItem>();
			for(int i = 0; i < data.size();i++){
				ReaderItem reader = data.get(i);
				if(reader.getTypename().equals(a)){
					list.add(reader);
				}else if(reader.getBorrowid().equals(str)){
					list.add(reader);
				}
			}
			if(!list.equals(null)){
				String[][] tbody = listToArray(list);
				return tbody;
			}else{
				return null;
			}
			
		}
		//集合数组转为二维数组
	  	private String[][] listToArray(ArrayList<ReaderItem> list) {
	  		String[][] tbody = new String[list.size()][10];
	  		for(int i = 0; i < list.size();i++){
	  			ReaderItem reader = list.get(i);
	  			tbody[i][0] = reader.getName();
	  			tbody[i][1] = reader.getSex();
	  			tbody[i][2] = reader.getMajor();
	  			tbody[i][3] = reader.getId();
	  			tbody[i][4] = reader.getBorrowid();
	  			tbody[i][5] = reader.getTel();
	  			tbody[i][6] = reader.getIdtype();
	  			tbody[i][7] = reader.getIdtime();
	  			tbody[i][8] = reader.getTypename();
	  		}
	  		return tbody;
	  	}
	
}
