package service;

import java.util.ArrayList;
import vo.AdministratorItem;
import dao.AdminDao;
/**
 * 管理员服务类
 */
public class AdminService {
	private AdminDao adminDao = new AdminDao();
	//查询服务
	public ArrayList<AdministratorItem> queryAdminItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<AdministratorItem> data = adminDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addAdminItem(String adminno,String name,String passwordid){
		AdministratorItem thisAdmin = new AdministratorItem(adminno,name,passwordid);
		if(adminDao.addAdminItem(thisAdmin)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updateAdminItem(String adminno,String name,String passwordid){
		ArrayList<AdministratorItem> data = queryAdminItem();
		for(int i = 0; i < data.size();i++){
			AdministratorItem admin = data.get(i);
			if(admin.getAdminno().equals(adminno)){
				boolean delSuccess = adminDao.delAdminItem(adminno);
				AdministratorItem thisAdmin = new AdministratorItem(adminno,name,passwordid);
				boolean addSuccess = adminDao.addAdminItem(thisAdmin);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delAdminItem(String number){
		ArrayList<AdministratorItem> data = queryAdminItem();
		for(int i = 0; i < data.size();i++){
			AdministratorItem admin = data.get(i);
			if(admin.getAdminno().equals(number)){
				if(adminDao.delAdminItem(number)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
	//搜索服务
}
