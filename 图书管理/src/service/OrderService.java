package service;

import java.util.ArrayList;
import vo.OrderBooksItem;
import dao.OrderDao;
/**
 * 管理员服务类
 */
public class OrderService {
	private OrderDao orderDao = new OrderDao();
	//查询服务
	public ArrayList<OrderBooksItem> queryOrderItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<OrderBooksItem> data = orderDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addOrderItem(String name,String number,String bookid,String author,String publishing,String price,String intime,String ordercounts, String administrator,String checkyn,String typename){
		OrderBooksItem thisOrder = new OrderBooksItem(name,number,bookid,author,publishing,Double.parseDouble(price),intime,Integer.parseInt(ordercounts),administrator,checkyn,typename);
		BooksService bookService = new BooksService();
		
		if(bookService.addBooksItem(name,number,bookid,author,publishing,price,ordercounts,typename)&&orderDao.addOrderItem(thisOrder)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	
}
