package service;

import java.util.ArrayList;
import vo.BooksTypeItem;
import dao.BookstypeDao;
/**
 * 管理员服务类
 */
public class BookstypeService {
	private BookstypeDao btDao = new BookstypeDao();
	//查询服务
	public ArrayList<BooksTypeItem> queryBooktypeItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<BooksTypeItem> data = btDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addBooktypeItem(String typename,String borrowdays){
		BooksTypeItem thisbt = new BooksTypeItem(typename,Integer.parseInt(borrowdays));
		if(btDao.addBooktypeItem(thisbt)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updateBooktypeItem(String typename,String borrowdays){
		ArrayList<BooksTypeItem> data = queryBooktypeItem();
		for(int i = 0; i < data.size();i++){
			BooksTypeItem bt = data.get(i);
			if(bt.getTypename().equals(typename)){
				boolean delSuccess = btDao.delBooktypeItem(typename);
				BooksTypeItem thisbt = new BooksTypeItem(typename,Integer.parseInt(borrowdays));
				boolean addSuccess = btDao.addBooktypeItem(thisbt);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delBooktypeItem(String typename){
		ArrayList<BooksTypeItem> data = queryBooktypeItem();
		for(int i = 0; i < data.size();i++){
			BooksTypeItem bt = data.get(i);
			if(bt.getTypename().equals(typename)){
				if(btDao.delBooktypeItem(typename)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
}
