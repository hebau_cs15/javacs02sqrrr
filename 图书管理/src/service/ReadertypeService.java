package service;

import java.util.ArrayList;
import vo.ReaderTypeItem;
import dao.ReadertypeDao;
/**
 * 管理员服务类
 */
public class ReadertypeService {
	private ReadertypeDao rtDao = new ReadertypeDao();
	//查询服务
	public ArrayList<ReaderTypeItem> queryReadertypeItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<ReaderTypeItem> data = rtDao.queryAllData();
		return data;		
	}
	//添加服务
	public boolean addReadertypeItem(String typename,String maxBorrowcounts){
		ReaderTypeItem thisrt = new ReaderTypeItem(typename,Integer.parseInt(maxBorrowcounts));
		if(rtDao.addReadertypeItem(thisrt)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updateReadertypeItem(String typename,String maxBorrowcounts){
		ArrayList<ReaderTypeItem> data = queryReadertypeItem();
		for(int i = 0; i < data.size();i++){
			ReaderTypeItem rt = data.get(i);
			if(rt.getTypename().equals(typename)){
				boolean delSuccess = rtDao.delReadertypeItem(typename);
				ReaderTypeItem thisrt = new ReaderTypeItem(typename,Integer.parseInt(maxBorrowcounts));
				boolean addSuccess = rtDao.addReadertypeItem(thisrt);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delReadertypeItem(String typename){
		ArrayList<ReaderTypeItem> data = queryReadertypeItem();
		for(int i = 0; i < data.size();i++){
			ReaderTypeItem rt = data.get(i);
			if(rt.getTypename().equals(typename)){
				if(rtDao.delReadertypeItem(typename)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
}
