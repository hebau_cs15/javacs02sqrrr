/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import dao.BookstypeDao;
import service.BookstypeService;
import vo.BooksTypeItem;

/**
 *
 * @author 七月
 */
public class BooktypeDialog extends javax.swing.JFrame {

    /**
     * Creates new form BooktypeDialog
     */
    public BooktypeDialog() {
        initComponents();
        queryBooktypeItem();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        typename = new javax.swing.JTextField();
        maxBorrowcounts = new javax.swing.JTextField();
        addBtn = new javax.swing.JButton();
        delBtn = new javax.swing.JButton();
        resetBtn = new javax.swing.JButton();
        updateBtn = new javax.swing.JButton();
        returnBtn = new javax.swing.JButton();
        exitBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("图书类型信息管理");
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "类型名", "借阅天数"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(80);
        }

        jLabel1.setText("类型名");

        jLabel2.setText("借阅天数");

        typename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typenameActionPerformed(evt);
            }
        });

        addBtn.setText("添加");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });

        delBtn.setText("删除");
        delBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delBtnActionPerformed(evt);
            }
        });

        resetBtn.setText("重置");
        resetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetBtnActionPerformed(evt);
            }
        });

        updateBtn.setText("修改");
        updateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateBtnActionPerformed(evt);
            }
        });

        returnBtn.setText("返回");
        returnBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnBtnActionPerformed(evt);
            }
        });

        exitBtn.setText("退出");
        exitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 587, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(maxBorrowcounts, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                                    .addComponent(typename))
                                .addGap(60, 60, 60)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(addBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(delBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE))
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(resetBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                    .addComponent(updateBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(returnBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(76, 76, 76)
                                .addComponent(exitBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(addBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(typename)
                    .addComponent(updateBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(maxBorrowcounts)
                    .addComponent(delBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(resetBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(returnBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(exitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(93, 93, 93))
        );

        pack();
    }// </editor-fold>                        

    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {                                       
        
	ArrayList<BooksTypeItem> data = booktypeService.queryBooktypeItem();
	Iterator<BooksTypeItem> iterator = data.iterator();
	while(iterator.hasNext()){
            BooksTypeItem bt = iterator.next();
            //如果存在重复则添加不成功
            if(bt.getTypename().equals(typename.getText())){
            JOptionPane.showMessageDialog(null, "图书类型不能重复，请检查数据");
            typename.setText("");
            maxBorrowcounts.requestFocus();
            }			
	}
        addBooktypeItem();
    }                                      

    private void exitBtnActionPerformed(java.awt.event.ActionEvent evt) {                                        
       System.exit(0);
    }                                       

    private void returnBtnActionPerformed(java.awt.event.ActionEvent evt) {                                          
        this.setVisible(false);
        new AdminDialog().setVisible(true);
    }                                         

    private void resetBtnActionPerformed(java.awt.event.ActionEvent evt) {                                         
        typename.setText("");
        maxBorrowcounts.setText("");
    }                                        

    private void typenameActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
    }                                        

    private void delBtnActionPerformed(java.awt.event.ActionEvent evt) {                                       
        delBooktypeItem();
    }                                      

    private void updateBtnActionPerformed(java.awt.event.ActionEvent evt) {                                          
         updateBooktypeItem();
    }     
  //查询方法
  	public void queryBooktypeItem(){
  		//定义表格头
  		String[] thead = {"类型名","借阅天数"};
  		//调用adminService的查询服务
  		ArrayList<BooksTypeItem> dataList = booktypeService.queryBooktypeItem();
  		//将查询到的集合转为数组，方便为JTable赋值
  		String[][] tbody = listToArray(dataList);
  		//将查询到的结果为table赋值
  		TableModel dataModel = new DefaultTableModel(tbody,thead);
  		jTable1.setModel(dataModel);		
  	}
  	//集合数组转为二维数组
  	private String[][] listToArray(ArrayList<BooksTypeItem> list) {
  		String[][] tbody = new String[list.size()][2];
  		for(int i = 0; i < list.size();i++){
  			BooksTypeItem bt = list.get(i);
  			tbody[i][0] = bt.getTypename();
  			tbody[i][1] = bt.getBorrowdays()+"";
  		}
  		return tbody;
  	}
    	//添加方法
	public void addBooktypeItem() {
		String typename1 = typename.getText();
		String borrowdays = maxBorrowcounts.getText();
		boolean aSuccess = booktypeService.addBooktypeItem(typename1,borrowdays);
		if(aSuccess){
			queryBooktypeItem(); //添加后刷新表格
			typename.setText("");
			maxBorrowcounts.setText("");
		}else{
			//没有添加成功弹窗错误提示
			JOptionPane.showMessageDialog(this, "添加失败");
		}		
	}
	//修改方法
	public void updateBooktypeItem() {
		String typename1 = typename.getText();
		String borrowdays = maxBorrowcounts.getText();
		boolean updateSuccess = new BookstypeDao().UpdateBooktypeItem(typename1,borrowdays);
		if(updateSuccess){
			queryBooktypeItem();
			typename.setText("");
			maxBorrowcounts.setText("");
		}else{
			JOptionPane.showMessageDialog(this, "修改失败，请检查数据");
		}		
	}
	//删除方法
	public void delBooktypeItem() {
		String delName = typename.getText();
		int delFlag = booktypeService.delBooktypeItem(delName);
		if(delFlag == 1){
			queryBooktypeItem();
			typename.setText("");
			maxBorrowcounts.setText("");
		}else if(delFlag == 2){
			JOptionPane.showMessageDialog(this, "没有这个图书类型，请检查数据");
		}else {
			JOptionPane.showMessageDialog(this, "删除失败");
		}
	}
    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify                     
    private javax.swing.JButton addBtn;
    private javax.swing.JButton delBtn;
    private javax.swing.JButton exitBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField maxBorrowcounts;
    private javax.swing.JButton resetBtn;
    private javax.swing.JButton returnBtn;
    private javax.swing.JTextField typename;
    private javax.swing.JButton updateBtn;
    private BookstypeService booktypeService=new BookstypeService();
    // End of variables declaration                   
}
