package dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.OrderBooksItem;
/**
 *  管理员数据访问类
 */
public class OrderDao {
	//获取所有数据
	public ArrayList<OrderBooksItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<OrderBooksItem> list = new ArrayList<OrderBooksItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select name,number,bookid,author,publishing,price,intime,ordercounts,adminno,checkyn,typename from OrderBooksItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				OrderBooksItem thisOrder = new OrderBooksItem();
				thisOrder.setName(rs.getString("name"));
				thisOrder.setNumber(rs.getString("number"));
				thisOrder.setBookid(rs.getString("bookid"));
				thisOrder.setAuthor(rs.getString("Author"));
				thisOrder.setPublishing(rs.getString("publishing"));
				thisOrder.setPrice(rs.getDouble("price"));
				thisOrder.setName(rs.getString("intime"));
				thisOrder.setName(rs.getString("ordercounts"));
				thisOrder.setAdminno(rs.getString("adminno"));
				thisOrder.setCheckyn(rs.getString("checkyn"));
				thisOrder.setTypename(rs.getString("typename"));
				list.add(thisOrder);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addOrderItem(OrderBooksItem order){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into OrderBooksItem  values (?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, order.getName());
			pstmt.setString(2, order.getNumber());
			pstmt.setString(3,order.getBookid());
			pstmt.setString(4,order.getAuthor());
			pstmt.setString(5,order.getPublishing());
			pstmt.setDouble(6,order.getPrice());
			pstmt.setString(7, order.getIntime());
			pstmt.setInt(8, order.getOrdercounts());
			pstmt.setString(9, order.getAdminno());
			pstmt.setString(10, order.getCheckyn());
			pstmt.setString(11, order.getTypename());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delOrderItem(String delNumber){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from OrderBooksItem where bookid=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delNumber);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
