package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.BooksTypeItem;
/**
 *  管理员数据访问类
 */
public class BookstypeDao {
	//获取所有数据
	public ArrayList<BooksTypeItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<BooksTypeItem> list = new ArrayList<BooksTypeItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select typename,borrowdays from BooksTypeItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				BooksTypeItem thisbt = new BooksTypeItem();
				thisbt.setTypename(rs.getString("typename"));
				thisbt.setBorrowdays(rs.getInt("borrowdays"));
				list.add(thisbt);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addBooktypeItem(BooksTypeItem bt){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into BooksTypeItem values (?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bt.getTypename());
			pstmt.setInt(2,bt.getBorrowdays());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delBooktypeItem(String delname){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from BooksTypeItem where typename=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delname);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
	
    //修改方法
	public boolean UpdateBooktypeItem(String a,String b){
		boolean result=false;
		Connection conn = null;	
		java.sql.Statement stmt = null;
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String  sql="update BooksTypeItem set borrowdays="+b+" where typename='"+a+"'";
			stmt.executeUpdate(sql);
			
			return true;
			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
