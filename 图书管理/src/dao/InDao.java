package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.InItem;
/**
 *  管理员数据访问类
 */
public class InDao {
	//获取所有数据
	public ArrayList<InItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<InItem> list = new ArrayList<InItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select intime,bookid from InItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				InItem thisrt = new InItem();
				thisrt.setIntime(rs.getString("intime"));
				thisrt.setNumber(rs.getString("bookid"));
				list.add(thisrt);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addInItem(InItem rt){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into InItem (intime,bookid) values (?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, rt.getIntime());
			pstmt.setString(2,rt.getBookid());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	/*public boolean delInItem(String delname){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from InItem where typename=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delname);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	*/
	//修改方法
		/*public boolean UpdateInItem(String a,String b){
			boolean result=false;
			Connection conn = null;	
			java.sql.Statement stmt = null;
			try{
				conn = JDBCUtils.getConnection(1);
				stmt = conn.createStatement();
				String  sql="update InItem set intime="+b+" where intime='"+a+"'";
				stmt.executeUpdate(sql);
				
				return true;
				
			}catch(Exception e ){
				e.printStackTrace();
			}finally{
				JDBCUtils.close(conn);
			}	
			return result;
		}	*/
}
