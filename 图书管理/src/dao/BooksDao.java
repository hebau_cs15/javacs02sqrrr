package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.BooksItem;
/**
 *  管理员数据访问类
 */
public class BooksDao {
	//获取所有数据
	public ArrayList<BooksItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<BooksItem> list = new ArrayList<BooksItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select name,number,bookid,author,publishing,price,counts,typename from BooksItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				BooksItem thisBook = new BooksItem();
				thisBook.setName(rs.getString("name"));
				thisBook.setNumber(rs.getString("number"));
				thisBook.setBookid(rs.getString("Bookid"));
				thisBook.setAuthor(rs.getString("author"));
				thisBook.setPublishing(rs.getString("publishing"));
				thisBook.setPrice(rs.getDouble("price"));
				thisBook.setCounts(rs.getInt("counts"));
				thisBook.setTypename(rs.getString("typename"));
				list.add(thisBook);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addBooksItem(BooksItem book){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into BooksItem (name,number,bookid,author,publishing,price,counts,typename) values (?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,book.getName());
			pstmt.setString(2, book.getNumber());
			pstmt.setString(3, book.getBookid());
			pstmt.setString(4, book.getAuthor());
			pstmt.setString(5, book.getPublishing());
			pstmt.setDouble(6,book.getPrice());
			pstmt.setInt(7,book.getCounts());
			pstmt.setString(8, book.getTypename());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//修改数据
	 
	//删除数据
	public boolean delBooksItem(String delNumber){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from BooksItem where number=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delNumber);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
