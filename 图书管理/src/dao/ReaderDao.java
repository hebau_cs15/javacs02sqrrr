package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.ReaderItem;
/**
 *  管理员数据访问类
 */
public class ReaderDao {
	//获取所有数据
	public ArrayList<ReaderItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<ReaderItem> list = new ArrayList<ReaderItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select name,sex,major,id,borrowid,tel,idtype,idtime,typename from ReaderItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				ReaderItem thisReader = new ReaderItem();
				thisReader.setName(rs.getString("name"));
				thisReader.setSex(rs.getString("sex"));
				thisReader.setMajor(rs.getString("major"));
				thisReader.setId(rs.getString("id"));
				thisReader.setBorrowid(rs.getString("Borrowid"));
				thisReader.setTel(rs.getString("tel"));
				thisReader.setIdtype(rs.getString("Idtype"));
				thisReader.setIdtime(rs.getString("idtime"));
				thisReader.setTypename(rs.getString("typename"));
				list.add(thisReader);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addReaderItem(ReaderItem reader){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into ReaderItem (name,sex,major,id,borrowid,tel,idtype,idtime,typename) values (?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, reader.getName());
			pstmt.setString(2, reader.getSex());
			pstmt.setString(3, reader.getMajor());
			pstmt.setString(4, reader.getId());
			pstmt.setString(5, reader.getBorrowid());
			pstmt.setString(6, reader.getTel());
			pstmt.setString(7, reader.getIdtype());
			pstmt.setString(8, reader.getIdtime());
			pstmt.setString(9, reader.getTypename());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delReaderItem(String delNumber){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from ReaderItem where borrowid=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delNumber);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
