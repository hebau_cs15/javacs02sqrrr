package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.ReaderTypeItem;
/**
 *  管理员数据访问类
 */
public class ReadertypeDao {
	//获取所有数据
	public ArrayList<ReaderTypeItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<ReaderTypeItem> list = new ArrayList<ReaderTypeItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select typename,maxBorrowcounts from ReaderTypeItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				ReaderTypeItem thisrt = new ReaderTypeItem();
				thisrt.setTypename(rs.getString("typename"));
				thisrt.setMaxBorrowcounts(rs.getInt("maxBorrowcounts"));
				list.add(thisrt);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addReadertypeItem(ReaderTypeItem rt){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into ReaderTypeItem (typename,maxBorrowcounts) values (?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, rt.getTypename());
			pstmt.setInt(2,rt.getMaxBorrowcounts());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delReadertypeItem(String delname){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from ReaderTypeItem where typename=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delname);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
	//修改方法
		public boolean UpdateBooktypeItem(String a,String b){
			boolean result=false;
			Connection conn = null;	
			java.sql.Statement stmt = null;
			try{
				conn = JDBCUtils.getConnection(1);
				stmt = conn.createStatement();
				String  sql="update ReaderTypeItem set maxBorrowcounts="+b+" where typename='"+a+"'";
				stmt.executeUpdate(sql);
				
				return true;
				
			}catch(Exception e ){
				e.printStackTrace();
			}finally{
				JDBCUtils.close(conn);
			}	
			return result;
		}	
}
