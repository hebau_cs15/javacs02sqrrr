package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.BorrowItem;
/**
 *  管理员数据访问类
 */
public class BorrowDao {
	//获取所有数据
	public ArrayList<BorrowItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<BorrowItem> list = new ArrayList<BorrowItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select borrowid,bookid,borrowtime,returntime from BorrowItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				BorrowItem thisBor = new BorrowItem();
				thisBor.setBorrowid(rs.getString("borrowid"));
				thisBor.setBooid(rs.getString("bookid"));
				thisBor.setBorrowtime(rs.getString("borrowtime"));
				thisBor.setReturntime(rs.getString("returntime"));
				list.add(thisBor);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addBorrowItem(BorrowItem bor){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into BorrowItem (borrowid,bookid,borrowtime,returntime) values (?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bor.getBorrowid());
			pstmt.setString(2,bor.getBookid());
			pstmt.setString(3, bor.getBorrowtime());
			pstmt.setString(4, bor.getReturntime());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delBorrowItem(String delNumber){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from BorrowItem where bookid=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delNumber);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
