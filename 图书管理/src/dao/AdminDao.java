package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.AdministratorItem;
/**
 *  管理员数据访问类
 */
public class AdminDao {
	//获取所有数据
	public ArrayList<AdministratorItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<AdministratorItem> list = new ArrayList<AdministratorItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select adminno,name,passwordid from AdministratorItem";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				AdministratorItem thisAdmin = new AdministratorItem();
				thisAdmin.setAdminno(rs.getString("adminno"));
				thisAdmin.setName(rs.getString("name"));
				thisAdmin.setPasswordid(rs.getString("passwordid"));
				list.add(thisAdmin);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//添加数据
	public boolean addAdminItem(AdministratorItem admin){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into AdministratorItem (adminno,name,passwordid) values (?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, admin.getAdminno());
			pstmt.setString(2,admin.getName());
			pstmt.setString(3,admin.getPasswordid());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delAdminItem(String delNumber){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from AdministratorItem where adminno=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delNumber);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
