package dbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * 负责数据库连接和关闭操作以及取得一个数据库的连接对象
 * 
 */
public class JDBCUtils {
	public static final int CONNECTION_MYSQL=2;
	public static final int CONNECTION_SQL=1;	
	public JDBCUtils() {	}
	/*
	 * 获取连接对象
	 */
	public static Connection getConnection(int connection_type)throws Exception
	{
		switch (connection_type)
		{
			case CONNECTION_SQL:
				return getConnectionSQL();
			case CONNECTION_MYSQL:
				return getConnectionMYSQL();
		}
		return null;
	}
	
	/*
	 * 连接SQLSERVER数据库
	 */	
	private static Connection getConnectionSQL()
	{
		Connection conn=null;
		final String DBDRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		final String DBURL = "jdbc:sqlserver://localhost:1433;databaseName=图书管理系统";
		final String DBUSER = "nam";
		final String DBPASS = "123456";		
		try {
			Class.forName(DBDRIVER);
			conn=DriverManager.getConnection(DBURL,DBUSER,DBPASS);
		} catch (ClassNotFoundException e) {				
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return conn;
	}	
	/*
	 * 连接MYSQL数据库
	 */
	private static Connection getConnectionMYSQL()
	{
		final String DBDRIVER = "com.mysql.jdbc.Driver";
		final String DBURL = "jdbc:mysql://localhost:3306/图书管理系统";
		final String DBUSER = "nam";
		final String DBPASS = "123456";
		Connection conn=null;		
		try
		{
			Class.forName(DBDRIVER); 			
			conn=DriverManager.getConnection(DBURL,DBUSER,DBPASS);			
		} catch (ClassNotFoundException e) {				
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return conn;
	}
	/*
	 * 关闭连接对象
	 */
	public static void close(Connection conn)
	{
		if(conn!=null)
		{
			try
			{
				conn.close();
			} catch(SQLException e)
			{
				e.printStackTrace();				
			}
			conn = null;
		}
	}	
}
