package vo;

public class BooksItem {
	private String name;
	private String number;
	private String bookid;
	private String author;
	private String publishing;
	private Double price;
	private Integer counts;
	private String typename;
	public BooksItem(){
		
	}
	public BooksItem(String name,String number,String bookid,String author,String publishing,Double price,Integer counts,String typename){
		this.name=name;
		this.number=number;
		this.bookid=bookid;
		this.author=author;
		this.publishing=publishing;
		this.price=price;
		this.counts=counts;
		this.typename=typename;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublishing() {
		return publishing;
	}
	public void setPublishing(String publishing) {
		this.publishing = publishing;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getCounts() {
		return counts;
	}
	public void setCounts(Integer counts) {
		this.counts = counts;
	}
	
}
