package vo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InItem {
	private String intime;
	private String bookid;
	SimpleDateFormat   formatter   = new   SimpleDateFormat( "yyyy-MM-dd ");
	public InItem(){
		
	}
	public InItem(String intime,String bookid){
		this.intime=formatter.format(new Date());
		this.bookid=bookid;
	}
	public String getBookid() {
		return bookid;
	}
	public void setNumber(String bookid) {
		this.bookid = bookid;
	}
	public String getIntime() {
		return intime;
	}
	public void setIntime(String intime) {
		this.intime = intime;
	}
	
}
