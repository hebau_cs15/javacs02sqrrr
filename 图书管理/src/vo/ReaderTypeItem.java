package vo;

public class ReaderTypeItem {
	private String typename;
	private Integer maxBorrowcounts;
	public ReaderTypeItem(){
		
	}
	public ReaderTypeItem(String typename,Integer maxBorrowcounts){
		this.typename=typename;
		this.maxBorrowcounts=maxBorrowcounts;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public Integer getMaxBorrowcounts() {
		return maxBorrowcounts;
	}
	public void setMaxBorrowcounts(Integer maxBorrowcounts) {
		this.maxBorrowcounts = maxBorrowcounts;
	}
	
}
