package vo;


import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderBooksItem {
	private String name;
	private String number;
	private String bookid;
	private String author;
	private String publishing;
	private Double price;
	private String intime;
	private Integer ordercounts;
	private String adminno;
	private String checkyn;
	private String typename;
	SimpleDateFormat   formatter   = new   SimpleDateFormat( "yyyy-MM-dd ");
	public OrderBooksItem(){
		
	}
	public OrderBooksItem(String name,String number,String bookid,String author,String publishing,Double price,String intime,Integer ordercounts, String adminno,String checkyn,String typename){
		this.name=name;
		this.number=number;
		this.bookid=bookid;
		this.author=author;
		this.publishing=publishing;
		this.price=price;
		this.intime=formatter.format(new Date());
		this.ordercounts=ordercounts;
		this.adminno=adminno;
		this.checkyn=checkyn;
		this.typename=typename;
	}
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublishing() {
		return publishing;
	}
	public void setPublishing(String publishing) {
		this.publishing = publishing;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getIntime() {
		return intime;
	}
	public void setIntime(String intime) {
		this.intime = intime;
	}
	public Integer getOrdercounts() {
		return ordercounts;
	}
	public void setOrdercounts(Integer ordercounts) {
		this.ordercounts = ordercounts;
	}
	public String getAdminno() {
		return adminno;
	}
	public void setAdminno(String adminno) {
		this.adminno = adminno;
	}
	public String getCheckyn() {
		return checkyn;
	}
	public void setCheckyn(String checkyn) {
		this.checkyn = checkyn;
	}
	
}
