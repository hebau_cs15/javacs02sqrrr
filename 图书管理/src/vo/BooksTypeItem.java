package vo;

public class BooksTypeItem {
	private String typename;
	private Integer borrowdays;
	public BooksTypeItem(){
		
	}
	public BooksTypeItem(String typename,Integer borrowdays){
		this.typename=typename;
		this.borrowdays=borrowdays;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public Integer getBorrowdays() {
		return borrowdays;
	}
	public void setBorrowdays(Integer borrowdays) {
		this.borrowdays = borrowdays;
	}
	
}
