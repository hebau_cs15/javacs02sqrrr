package vo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import dbc.JDBCUtils;

public class AdministratorItem {
	private String adminno;
	private String name;
	private String passwordid;
	public AdministratorItem(){
		
	}
	public AdministratorItem(String adminno,String name,String passwordid){
		this.adminno=adminno;
		this.name=name;
		this.passwordid=passwordid;
	}
	public String getAdminno() {
		return adminno;
	}
	public void setAdminno(String adminno) {
		this.adminno = adminno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPasswordid() {
		return passwordid;
	}
	public void setPasswordid(String passwordid) {
		this.passwordid = passwordid;
	}
	
}
