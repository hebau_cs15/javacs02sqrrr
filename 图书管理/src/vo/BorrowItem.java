package vo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BorrowItem {
	private String borrowid;
	private String bookid;
	private String borrowtime;
	private String returntime;
	SimpleDateFormat   formatter   = new   SimpleDateFormat( "yyyy-MM-dd ");
	public BorrowItem(){
		
	}
	public BorrowItem(String borrowid,String bookid,String borrowtime,String returntime){
		this.borrowid=borrowid;
		this.bookid=bookid;
		this.borrowtime=formatter.format(new Date());
		this.returntime=formatter.format(new Date());
	}
	public String getBorrowid() {
		return borrowid;
	}
	public void setBorrowid(String borrowid) {
		this.borrowid = borrowid;
	}
	public String getBookid() {
		return bookid;
	}
	public void setBooid(String bookid) {
		this.bookid = bookid;
	}
	public String getBorrowtime() {
		return borrowtime;
	}
	public void setBorrowtime(String borrowtime) {
		this.borrowtime=formatter.format(new Date());
	}
	public String getReturntime() {
		return returntime;
	}
	public void setReturntime(String returntime) {
		this.returntime=formatter.format(new Date());
	}
}
