package vo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReaderItem {
	private String name;
	private String sex;
	private String major;
	private String id;
	private String borrowid;
	private String tel;
	private String idtype;
	private String idtime;
	private String typename;
	SimpleDateFormat   formatter   = new   SimpleDateFormat( "yyyy-MM-dd ");
	public ReaderItem(){
		
	}
	public ReaderItem(String name,String sex,String major,String id,String borrowid,String tel,String idtype,String idtime,String typename){
		this.name=name;
		this.sex=sex;
		this.major=major;
		this.id=id;
		this.borrowid=borrowid;
		this.tel=tel;
		this.idtype=idtype;
		this.idtime=formatter.format(new Date());
		this.typename=typename;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBorrowid() {
		return borrowid;
	}
	public void setBorrowid(String borrowid) {
		this.borrowid = borrowid;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getIdtype() {
		return idtype;
	}
	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}
	public String getIdtime() {
		return idtime;
	}
	public void setIdtime(String idtime) {
		this.idtime=formatter.format(new Date());
	}
	
}
