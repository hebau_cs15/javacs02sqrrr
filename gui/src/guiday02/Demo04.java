package guiday02;
import java.awt.Graphics;


import com.neusoft.gui.Constant;
/*
 * 物体沿椭圆轨道运动
 * 
 */
import com.neusoft.gui.IFrame;
public class Demo04 extends IFrame{
	
	private int x = 100;
	private int y = 50;
	private int width = 15;
	private int height = 15;
	private double spead = 0.1;
	
	private double longAxis = 300;
	private double shortAxis = 200;
	
	private double degree;//角度只能使用弧度制
	
	
	public static void main(String[] args){
		new Demo04().launchFrame();
	}
	

	@Override
	public void paint(Graphics g) {
		//画
		g.fillRect(x, y, width, height);
		//动
		move();
		//画轨道
		drawTrace(g);
		
	}
	
	public void move(){
		x = Constant.GAME_WIDTH/2 + (int)(longAxis * Math.cos(degree)) - width/2;
		y = Constant.GAME_HEIGHT/2 + (int)(shortAxis * Math.sin(degree)) -height/2;
		//物理学中角度是速度的增量
		degree += spead;
	}
	
	public void drawTrace(Graphics g){
		int x = (int) (Constant.GAME_WIDTH/2 - longAxis);
		int y = (int) (Constant.GAME_HEIGHT/2 - shortAxis);
		int width = (int) (longAxis*2);
		int height = (int) (shortAxis*2);
		g.drawOval(x, y, width, height);
	}
}
