package guiday02;
import java.awt.Graphics;

import com.neusoft.gui.Constant;
/*
 * 物体沿任意角度运动
 */
import com.neusoft.gui.IFrame;
public class Demo03 extends IFrame{
	
	private int x = 100;
	private int y = 50;
	private int width = 15;
	private int height = 15;
	private double spead = 15;
	private double degree = Math.PI/4;//角度只能使用弧度制
	private boolean right = true;
	
	public static void main(String[] args){
		new Demo03().launchFrame();
	}
	
	@Override
	public void paint(Graphics g) {
		
		if(spead <= 0){
			spead = 0;
		}
		
		g.fillRect(x, y, width, height);
		
		x += (int)(spead * Math.cos(degree));
		y += (int)(spead * Math.sin(degree));
		
		if(x<0 || x>Constant.GAME_WIDTH){
			degree = Math.PI-degree ;
		}
		if(y<0 || y>Constant.GAME_WIDTH){
			degree = -degree ;
		}
		
		spead -= 0.1;
	}
	
}
