package guiday02;

import java.awt.Graphics;

import com.neusoft.gui.Constant;
import com.neusoft.gui.IFrame;

public class Demo02 extends IFrame{

	private int x = 50;
	private int y = 50;
	private int width = 15;
	private int height = 15;
	private boolean right = true;
	
	public static void main(String[] args) {
		new Demo02().launchFrame();
	}

	public void paint(Graphics g){
		g.fillRect(x, y, width, height);
		//改变方向时如何运动
		if(right){
			x += 15;
		}else{
			x -= 15;
		}
		if(right){
			y +=15;
		}
		else{
			y -= 15;
		}
		//判断方向的改变
		if(x>Constant.GAME_WIDTH){
			right = false;
		}if(x<0){
			right = true;
		}
		if(y>Constant.GAME_HEIGHT){
			right = false;
		}if(y<0){
			right = true;
		}
	}
}
