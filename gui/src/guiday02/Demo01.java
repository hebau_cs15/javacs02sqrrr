package guiday02;

import java.awt.Graphics;

import com.neusoft.gui.IFrame;

public class Demo01 extends IFrame{
	
	public static void main(String[] args){
		new Demo01().launchFrame();
	}


	private int x = 50;
	private int y = 500;
	private int width = 15;
	private int height = 15;
	
	@Override
	public void paint(Graphics g){
		g.fillRect(x, y, width, height);
		x += 1;
		y -= 1;
	}
	
	
	
}
