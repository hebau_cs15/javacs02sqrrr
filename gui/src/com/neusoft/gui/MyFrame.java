package com.neusoft.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/*
 * GUI(图形用户界面)
 * java.awt.*+java.swing.*
 * 目的：熟悉面向对象编程的思维模式，熟悉接口，抽象类，继承，封装，多态，等面向对象核心内容
 * 掌握多线程编程，了解静态内部类，成员内部类的使用
 * 熟练掌握容器中的内容
 * 掌握I/O流加载文件
 * 了解java.awt包中类及对应方法的使用
 * 
 * 内部类的分类：
 * 1.成员内部类√
 * 2.静态内部类
 * 3.局部内部类
 * 4.匿名内部类√
 * 
 * @author somnus
 * GUI编程过程：
 *   1、继承frame类
 *   2、重写paint()方法
 *   
 *  
 */

public class MyFrame extends Frame{
	
	public static void main(String args[]){
		MyFrame myframe =  new MyFrame();
		myframe.launchFrame();
	}
	
	
	
	
	
	//1调出窗口
	public void launchFrame(){
		//对当前窗口属性设置
		//设置窗口大小500*300
		this.setSize(1000,1000);
//		//设置窗口显示的初始位置在（100，100）
//		this.setLocation(100,100);
		//相对于屏幕居中
		this.setLocationRelativeTo(null);
		
		//设置窗口的可见性默认值（不可见）
		this.setVisible(true);
		//重写窗口关闭的方法,添加窗口监听器
		//匿名内部类 通常当一个参数是接口或者抽象类的时候，使用匿名内部类，以便节省资源
		//抽象类不能new 
		this.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
		
		//设置标题
		this.setTitle("MyFrame飞机大战");
		//设置窗口不可修改大小   默认值为true
		this.setResizable(false);
	}
	
	//Graphics 画笔
	
	public void paint(Graphics g){
		//直线
		g.drawLine(50, 50, 200, 200);
		//矩形
		g.drawRect(80, 80, 200, 200);
		g.drawRoundRect(400, 400, 100, 100, 20, 20);
//		g.draw3DRect(50, 50, 200, 200, true);//x y weight height boolean
//		g.draw3DRect(100, 100, 200, 200, true);
		
		//修改颜色
		Color c = g.getColor();//获取当前的系统颜色
		g.setColor(new Color(64,64,64));
		
		//椭圆 矩形的内切圆
		g.drawOval(80, 80, 200, 300);
		//实心圆
		g.fillOval(100, 100, 200, 200);
		
		//把颜色恢复为系统默认
		g.setColor(c);
		
		
		Font f = getFont();
		g.setColor(Color.BLUE);
		g.setFont(new Font("微软雅黑",Font.BOLD,50));
		//写字符串
		g.drawString("neirong", 200, 600);//内容 x y坐标
	}
}
