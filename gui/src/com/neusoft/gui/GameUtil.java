package com.neusoft.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.nio.Buffer;

import javax.imageio.ImageIO;

/*
 * 工具类 尽量使用静态方法
 */
public class GameUtil {
	//获取图片的方法
	public static Image getImage(String impath){
		//URL唯一资源定位符
		URL u = GameUtil.class.getClassLoader().getResource(impath);
		//       当前类   反射      类方法           获取资源
		
		BufferedImage img = null;
		try {
			//通过内存对象传入IO流当中
			img = ImageIO.read(u);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return img;
	}
	
	
}
