package com.neusoft.gui;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/*
 * 所有窗口类的父类
 * 
 */
public class IFrame extends Frame{
	public void launchFrame(){
		this.setSize(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
		this.setTitle(Constant.GAME_TITLE);
		this.setResizable(false);
		new MyThread().start();
	}
	
	//自定义线程内部类
	class MyThread extends Thread{
		public void run(){
			while(true){
				//java.awt包提供了一个方法 无限调用paint(g)方法
				repaint();
				//让线程休眠一段时间
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
//	/
	
}
