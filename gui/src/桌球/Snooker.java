package ����;

import java.awt.Graphics;

import com.neusoft.gui.Constant;
import com.neusoft.gui.IFrame;

public class Snooker extends IFrame{

	public static void main(String[] args) {
		new Snooker().launchFrame();
	}

	private int x = 50;
	private int y = 50;
	private int width = 30;
	private int height = 30;
	private double degree = Math.PI/6;
	private double spead = 5;
	
	@Override
	public void paint(Graphics g) {
		g.fillOval(x, y, width, height);
		
		if(spead<=0){
			spead = 0;
		}
		
		x += (int)(spead * Math.cos(degree));
		y += (int)(spead * Math.sin(degree));
		
		if(x<0 || x>Constant.GAME_WIDTH-30){
			degree = Math.PI-degree;
		}
		if(y<30 || y>Constant.GAME_HEIGHT-30){
			degree = -degree;
		}
		spead -= 0.01;
	}
}
