package ��Ҫͨ��;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int count = input.nextInt();
		if(count>10){
			System.exit(0);
		}
		String[] str = new String[count];
		for(int i=0 ; i<count ; i++){
			str[i] = input.next();
		}
		String[] yn = new String[count];
		for(int i=0 ; i<count ; i++){
			yn[i] = check(str[i]);
			System.out.println(yn[i]);
		}
	}
	
	public static String check(String str){
		String regex1 = "\\s*PA+T\\s*";
		String regex2 = "A*PA{0,1}TA*";
		Pattern pat1 = Pattern.compile(regex1);
		Matcher mat1 = pat1.matcher(str);
		Pattern pat2 = Pattern.compile(regex2);
		Matcher mat2 = pat2.matcher(str);
		if(mat1.matches()||mat2.matches()){
			return "YES";
		}else {			
			return "NO";
		}
	}
}
