package 数组元素循环右移问题;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		if(n>100||n<0){
			System.exit(0);
		}
		int m = input.nextInt();
		int digit[] = new int[n];
		for(int i=0 ; i<n ; i++){
			digit[i] = input.nextInt();
		}
		for(int i=0,j=0 ; i<n ; i++,j++){
			if(j>=(m%n)){
				j=0;
			}
			int index = (i+m)%n;
			if(index != i){
				int temp = digit[j];
				digit[j] = digit[index];
				digit[index] = temp;
			}
		}
		for(int i=0 ; i<n ; i++){
			System.out.print(digit[i]);
			if(i != n-1){
				System.out.print(" ");
			}
		}
	}

}
