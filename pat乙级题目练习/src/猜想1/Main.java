package ����1;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int a = input.nextInt();
		if(a<=0 || a>1000){
			System.exit(0);
		}
		int count=0;
		while(a!=1){
			if(a%2==0){
				a=a/2;
			}else if(a%2!=0){
				a=(3*a+1)/2;
			}
			count++;
		}
		System.out.println(count);
	}

}
