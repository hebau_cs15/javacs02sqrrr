package 换个格式输出整数;

import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int digit = input.nextInt();
		if(digit<0||digit>1000){
			System.exit(0);
		}
		int[] count = new int[3];
		count[0] = digit/100;
		count[1] = (digit/10)%10;
		count[2] = digit%10;
		switch(count[0]){
		case 1:System.out.print("B");break;
		case 2:System.out.print("BB");break;
		case 3:System.out.print("BBB");break;
		case 4:System.out.print("BBBB");break;
		case 5:System.out.print("BBBBB");break;
		case 6:System.out.print("BBBBBB");break;
		case 7:System.out.print("BBBBBBB");break;
		case 8:System.out.print("BBBBBBBB");break;
		case 9:System.out.print("BBBBBBBBB");break;
		}
		switch(count[1]){
		case 1:System.out.print("S");break;
		case 2:System.out.print("SS");break;
		case 3:System.out.print("SSS");break;
		case 4:System.out.print("SSSS");break;
		case 5:System.out.print("SSSSS");break;
		case 6:System.out.print("SSSSSS");break;
		case 7:System.out.print("SSSSSSS");break;
		case 8:System.out.print("SSSSSSSS");break;
		case 9:System.out.print("SSSSSSSSS");break;
		}
		switch(count[2]){
		case 1:System.out.print("1");break;
		case 2:System.out.print("12");break;
		case 3:System.out.print("123");break;
		case 4:System.out.print("1234");break;
		case 5:System.out.print("12345");break;
		case 6:System.out.print("123456");break;
		case 7:System.out.print("1234567");break;
		case 8:System.out.print("12345678");break;
		case 9:System.out.print("123456789");break;
		}
	}

}
