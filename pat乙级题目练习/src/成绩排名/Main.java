package �ɼ�����;
import java.util.Scanner;


class Stu implements Comparable<Stu>{
	private String name;
	private String no;
	private int score;
	
	
	
	
	public Stu(){
		
	}
	
	public Stu(String name,String no,int score){
		this.name=name;
		this.no=no;
		this.score=score;
	}
	
	
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	
	
	public int compareTo(Stu stu){
		if(this.score>stu.score){
			return -1;
		}else if(this.score<stu.score){
			return 1;
		}else{
			return 0;
		}
	}
	public String toString(){
		return name+" "+no;
	}
}



public class Main {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		int num = input.nextInt();
		
		Stu[] stu = new Stu[num];
		
		for(int i=0 ; i<num ; i++){
			String name = input.next();
			String no = input.next();
			int score = input.nextInt();
			Stu student = new Stu(name,no,score);
			stu[i]=student;
		}
		
		
		java.util.Arrays.sort(stu);
		System.out.println(stu[0].toString());
		System.out.println(stu[num-1].toString());
	}

}
