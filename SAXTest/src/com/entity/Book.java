package com.entity;

public class Book {
	private String id;
	private String name;
	private String author;
	private String year;
	private String language;
	private String price;
	public Book(){
		
	}
	public Book(String id,String name,String author,String year,String language,String price){
		this.id=id;
		this.name=name;
		this.author=author;
		this.year=year;
		this.language=language;
		this.price=price;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
