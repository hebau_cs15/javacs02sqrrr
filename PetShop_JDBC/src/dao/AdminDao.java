package dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dbc.JDBCUtils;
import vo.PetItem;
/**
 *  管理员数据访问类
 */
public class AdminDao {
	//获取所有数据
	public ArrayList<PetItem> queryAllData(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<PetItem> list = new ArrayList<PetItem>();
		try{
			conn = JDBCUtils.getConnection(1);
			stmt = conn.createStatement();
			String sql = "select number,name,price,amount,age from pet";
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				PetItem thispet = new PetItem();
				thispet.setNumber(rs.getString("number"));
				thispet.setName(rs.getString("name"));
				thispet.setPrice(rs.getDouble("price"));
				thispet.setAmount(rs.getInt("amount"));
				thispet.setAge(rs.getDouble("age"));
				list.add(thispet);				
			}
			return list;
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return null;
	}
	//根据水果编号进行查询
	public PetItem queryData(String number){
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PetItem thisPet = new PetItem();
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "select number,name,price,amount,age from pet where number = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, number);
			rs = pstmt.executeQuery();	
			if(rs == null)
				thisPet = null;
			if(rs.next()){				
				thisPet.setNumber(rs.getString("number"));
				thisPet.setName(rs.getString("name"));
				thisPet.setPrice(rs.getDouble("price"));
				thisPet.setAmount(rs.getInt("amount"));	
				thisPet.setAge(rs.getDouble("age"));	
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}
		return thisPet;		
	}
	//添加数据
	public boolean addPetItem(PetItem pet){
		Connection conn = null;
		PreparedStatement pstmt = null;	
		boolean result=false;
		try{
			conn = JDBCUtils.getConnection(1);
			String sql = "insert into pet (number,name,price,amount,age) values (?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, pet.getNumber());
			pstmt.setString(2,pet.getName());
			pstmt.setDouble(3,pet.getPrice());
			pstmt.setInt(4,pet.getAmount());
			pstmt.setInt(5,pet.getAmount());
			int num = pstmt.executeUpdate();
			if(num > 0){
				result = true;
			}			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}
	//删除数据
	public boolean delPetItem(String delNumber){
		boolean result=false;
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try{
			conn = JDBCUtils.getConnection(1);
			String  sql="delete from pet where number=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, delNumber);
			if(pstmt.executeUpdate()>0){
				result = true;
			}					
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			JDBCUtils.close(conn);
		}	
		return result;
	}	
}
