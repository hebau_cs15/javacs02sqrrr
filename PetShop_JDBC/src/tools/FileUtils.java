package tools;

import java.util.Date;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import vo.SellPetItem;

public class FileUtils {
	public static final String SEPARATE_FIELD = ",";  //字段分隔，英文逗号
	public static final String SEPARATE_LINE = "\r\n"; //行分隔
	/**
	 * 保存水果信息
	 */
	public static void saveFruits(SellPetItem fruit) {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String name = "销售记录" + format.format(date)+ ".csv";
		InputStream in = null;
		try {
			in = new FileInputStream(name);  //判断本地是否存在文件
			if(in != null){
				in.close();
			    createFile(name,true,fruit);  // 存在文件，采用修改文件方式
			}
		} catch (FileNotFoundException e) {
			createFile(name,false,fruit);  // 不存在文件，采用新建文件方式
		} catch (IOException e) {			
			e.printStackTrace();
		}		
	}
	
    /**
     * 将售出信息保存到本地
     * @param name 文件名
     * @param label 文件已存在标识 true：存在则修改 false：不存在则新建
     * @param fruit 水果信息
     */
	public static void createFile(String name,boolean label,SellPetItem fruit){
		BufferedOutputStream out = null;  //字节缓冲流
		StringBuffer sbf = new StringBuffer();
		try {
			if(label){
				out = new BufferedOutputStream(new FileOutputStream(name,true));
			}else{
				out = new BufferedOutputStream(new FileOutputStream(name));
				String[] fieldSort = {"水果编号","水果名称","单价","购买数量","总价"};
				//创建表头
				for (String fieldKey : fieldSort){
					sbf.append(fieldKey).append(SEPARATE_FIELD);
			    }
			}
			sbf.append(SEPARATE_LINE);
			sbf.append(fruit.getNumber()).append(SEPARATE_FIELD);
			sbf.append(fruit.getName()).append(SEPARATE_FIELD);
			sbf.append(fruit.getPrice()).append(SEPARATE_FIELD);
			sbf.append(fruit.getAmount()).append(SEPARATE_FIELD);
			sbf.append(fruit.getMoney()).append(SEPARATE_FIELD);
			String str = sbf.toString();
			byte[] b = str.getBytes();
			for(int i = 0; i < b.length;i++){
				out.write(b[i]);
			}
		}catch (Exception e) {
				e.printStackTrace();
		}finally{
			if(out != null)
			{
				try {
					out.close();
				} catch (IOException e) {					
					e.printStackTrace();
				}
			}			
		}		
	}	
}
