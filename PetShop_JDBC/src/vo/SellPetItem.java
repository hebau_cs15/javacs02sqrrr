package vo;

public class SellPetItem {
	private String number;
	private String name;
	private Double price;
	private int amount;
	private Double age;
	private Double money;
	public SellPetItem(String number, String name, Double price, int amount,Double age,
			Double money) {
		super();
		this.number = number;
		this.name = name;
		this.price = price;
		this.amount = amount;
		this.age = age;
		this.money = money;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public Double getAge() {
		return age;
	}
	public void setAge(Double age) {
		this.age = age;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoner(Double money) {
		this.money = money;
	};
}
