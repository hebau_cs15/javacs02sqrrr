package vo;
/**
 * 水果项数据模型--实体类
 */
public class PetItem {
	private String number;
	private String name;
	private Double price;
	private int amount;
	private Double age;
	public PetItem() {		
	}
	public PetItem(String number, String name, Double price, int amount,Double age) {
		super();
		this.number = number;
		this.name = name;
		this.price = price;
		this.amount = amount;
		this.age = age;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Double getAge() {
		return age;
	}
	public void setAge(Double age) {
		this.age = age;
	}
	public String toString() {
		return "宠物编号：" + number + ", 宠物名称：" + name + ", 价格："
				+ price + ",数量：" + amount+ ",年龄：" + age ;
	}
		
}
