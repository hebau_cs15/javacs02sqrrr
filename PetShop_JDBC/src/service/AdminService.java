package service;

import java.util.ArrayList;
import vo.PetItem;
import dao.AdminDao;
/**
 * 管理员服务类
 */
public class AdminService {
	private AdminDao adminDao = new AdminDao();
	//查询服务
	public ArrayList<PetItem> queryPetItem(){
		//调用Dao层的方法获取所有数据
		ArrayList<PetItem> data = adminDao.queryAllData();
		return data;		
	}
	//按编号查詢
	public PetItem queryData(String number){
		PetItem pet = adminDao.queryData(number);
		return pet;
	}
	//添加服务
	public boolean addPetItem(String number,String name,String price,String amout,String age){
		PetItem thisFruit = new PetItem(number,name,Double.parseDouble(price),Integer.parseInt(amout),Double.parseDouble(age));
		if(adminDao.addPetItem(thisFruit)){  //添加成功
			return true;
		}else{
			return false;
		}		
	}
	//修改服务
	public boolean updatePetItem(String number,String name,String price,String amount,String age){
		ArrayList<PetItem> data = queryPetItem();
		for(int i = 0; i < data.size();i++){
			PetItem pet = data.get(i);
			if(pet.getNumber().equals(number)){
				boolean delSuccess = adminDao.delPetItem(number);
				PetItem thisPet = new PetItem(number,name,Double.parseDouble(price),Integer.parseInt(amount),Double.parseDouble(age));
				boolean addSuccess = adminDao.addPetItem(thisPet);
				if(delSuccess && addSuccess){
					return true;
				}				
			}			
		}
		return false;
	}
	//删除服务
	public int delPetItem(String number){
		ArrayList<PetItem> data = queryPetItem();
		for(int i = 0; i < data.size();i++){
			PetItem pet = data.get(i);
			if(pet.getNumber().equals(number)){
				if(adminDao.delPetItem(number)){
					return 1;
				}else{
					return 0;
				}
			}
		}
		return 2;	
	}
}
