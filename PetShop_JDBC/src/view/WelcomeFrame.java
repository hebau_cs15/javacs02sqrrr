package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tools.GUITools;
/**
 * 欢迎窗口
 */

@SuppressWarnings("serial")
public class WelcomeFrame extends JFrame{
	private JLabel titleLabel = new JLabel(new ImageIcon("fruit.jpg"));
	private JButton btn = new JButton("进入系统");
	//构造方法
	public WelcomeFrame(){
		this.init();                 //初始化
		this.addComponent();         //添加组件
		this.addListener();          //添加监听器
	}	
	//窗口初始化
	private void init() {
		this.setTitle("宠物商店欢迎你！");
		this.setSize(450,400);
		GUITools.center(this);    //窗口居中
		GUITools.setTitleImage(this,"title.png");  // 显示窗口图标
		this.setResizable(false);   //窗体大小固定
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}
	//添加组件
	private void addComponent() {
		this.add(this.titleLabel,BorderLayout.NORTH);
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(null);  // 清除JPanel的布局
		this.add(btnPanel);
		btn.setBounds(180,20,120,50);  //定义按钮边界位置
		btnPanel.add(btn,BorderLayout.CENTER);		
	}
	//添加监听器
	private void addListener() {	
		btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showAdminDialog();
			}
		});		
	}
	//显示管理员界面
	public void showAdminDialog(){
		this.setVisible(false);
		new AdminDialog().setVisible(true);
	}

}
