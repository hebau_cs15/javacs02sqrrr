package view;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import service.AdminService;
import tools.FileUtils;
import tools.GUITools;
import vo.PetItem;
import vo.SellPetItem;
/**
 * 管理员窗口类
 */

@SuppressWarnings("serial")//取消警告
public class AdminDialog extends JDialog{
	//定义界面使用的组件
	private JLabel tableLabel = new JLabel("宠物列表");
	private JScrollPane tablePane = new JScrollPane();
	private JTable table = new JTable();
	private JLabel numberLabel = new JLabel("宠物编号");
	private JLabel nameLabel = new JLabel("宠物名称");
	private JLabel priceLabel = new JLabel("价格");
	private JLabel unitLabel = new JLabel("数量");
	private JLabel ageLabel = new JLabel("年龄");
	private JTextField addNumberText = new JTextField(6);
	private JTextField addNameText = new JTextField(6);
	private JTextField addPriceText = new JTextField(6);
	private JTextField addUnitText = new JTextField(6);
	private JTextField addAgeText = new JTextField(6);
	private JButton addBtn = new JButton("添加宠物");
	private JTextField updateNumberText = new JTextField(6);
	private JTextField updateNameText = new JTextField(6);
	private JTextField updatePriceText = new JTextField(6);
	private JTextField updateUnitText = new JTextField(6);
	private JTextField updateAgeText = new JTextField(6);
	private JButton updateBtn = new JButton("修改宠物");
	private JTextField delNumberText = new JTextField(6);
	private JButton delBtn = new JButton("删除宠物");
	private JButton buyBtn = new JButton("购买宠物");
	private JLabel buyNumberLabel = new JLabel("宠物编号");
	private JLabel buyLabel = new JLabel("数量");
	private JTextField buyNumberText = new JTextField(6);
	private JTextField buyAmountText = new JTextField(6);
	private JButton exitBtn = new JButton("退出系统");
	private AdminService adminService = new AdminService();  // 定义服务类对象，提供完整功能服务
	//构造方法
	public AdminDialog(){
		this(null,true);		
	}
	//构造方法
	public AdminDialog(Frame owner,boolean modal){
		super(owner,modal);
		this.init();            //初始化
		this.addComponent();    //添加组件
		this.addListener();     //添加监听器
		queryPetItem();       //创建对象时显示数据
	}
	//初始化操作
	private void init() {
		this.setTitle("宠物商店管理");
		this.setSize(700,500);
		GUITools.center(this);
		this.setResizable(false);
	}
	//添加组件
	private void addComponent() {
		this.setLayout(null);
		tableLabel.setBounds(265,20,70,25);
		this.add(tableLabel);
		//表格
		table.getTableHeader().setReorderingAllowed(false);//列不能移动
		table.getTableHeader().setResizingAllowed(false);//不可拉动表格
		table.setEnabled(false);  //不可更改数据
		tablePane.setBounds(50, 50, 500, 200);
		tablePane.setViewportView(table);//视口装入表格
		this.add(tablePane);
		//字段标题
		numberLabel.setBounds(50, 250, 70, 25);
		nameLabel.setBounds(150, 250, 70, 25);
		priceLabel.setBounds(250, 250, 70, 25);
		unitLabel.setBounds(350, 250, 70, 25);
		ageLabel.setBounds(450, 250, 70, 25);
		this.add(numberLabel);
		this.add(nameLabel);
		this.add(priceLabel);
		this.add(unitLabel);
		this.add(ageLabel);
		//添加组件
		addNumberText.setBounds(50, 280, 80, 25);
		addNameText.setBounds(150, 280, 80, 25);
		addPriceText.setBounds(250, 280, 80, 25);
		addUnitText.setBounds(350, 280, 80, 25);
		addAgeText.setBounds(450, 280, 80, 25);
		this.add(addNumberText);
		this.add(addNameText);
		this.add(addPriceText);
		this.add(addUnitText);
		this.add(addAgeText);
		addBtn.setBounds(560, 280, 90, 25);
		this.add(addBtn);
		//修改组件
		updateNumberText.setBounds(50, 310, 80, 25);
		updateNameText.setBounds(150, 310, 80, 25);
		updatePriceText.setBounds(250, 310, 80, 25);
		updateUnitText.setBounds(350, 310, 80, 25);
		updateAgeText.setBounds(450, 310, 80, 25);
		this.add(updateNumberText);
		this.add(updateNameText);
		this.add(updatePriceText);
		this.add(updateUnitText);
		this.add(updateAgeText);
		updateBtn.setBounds(560, 310, 90, 25);
		this.add(updateBtn);
		//删除组件
		delNumberText.setBounds(350, 340, 80, 25);
		this.add(delNumberText);		
		delBtn.setBounds(560, 340, 90, 25);
		this.add(delBtn);
		exitBtn.setBounds(250, 400, 120, 50);
		this.add(exitBtn);
		//购买组件
		buyNumberLabel.setBounds(50, 370, 70, 25);
		this.add(buyNumberLabel);
		buyNumberText.setBounds(150, 370, 80, 25);
		this.add(buyNumberText);
		buyLabel.setBounds(250, 370, 70, 25);
		this.add(buyLabel);
		buyAmountText.setBounds(350, 370, 80, 25);
		this.add(buyAmountText);
		buyBtn.setBounds(560,370,90,25);
		this.add(buyBtn);
	}
	//添加监听器
	private void addListener() {
		//设置添加编号文本框监听
		addNumberText.addFocusListener(new FocusAdapter(){
			public void focusLost(FocusEvent e){
				ArrayList<PetItem> data = adminService.queryPetItem();
				Iterator<PetItem> iterator = data.iterator();
				while(iterator.hasNext()){
					PetItem fruit = iterator.next();
					//如果存在重复编号则添加不成功
					if(fruit.getNumber().equals(addNumberText.getText())){
						JOptionPane.showMessageDialog(null, "宠物编号不能重复，请检查数据");
						addNumberText.setText("");
						addNumberText.requestFocus();
					}			
				}
			}
		});
		//设置购买编号文本框监听
		buyNumberText.addFocusListener(new FocusAdapter(){
			public void focusLost(FocusEvent e){
				ArrayList<PetItem> data = adminService.queryPetItem();
				Iterator<PetItem> iterator = data.iterator();
				while(iterator.hasNext()){
					PetItem fruit = iterator.next();					
					if(fruit.getNumber().equals(buyNumberText.getText())){
						return;						
					}			
				}
				JOptionPane.showMessageDialog(null, "没有这种宠物，请检查数据");
				buyNumberText.setText("");
				buyNumberText.requestFocus();
			}
		});
/*		//设置购买数量文本框监听
		buyAmountText.addFocusListener(new FocusAdapter(){
			public void focusLost(FocusEvent e){
				ArrayList<FruitItem> data = adminService.queryFruitItem();
				Iterator<FruitItem> iterator = data.iterator();
				while(iterator.hasNext()){
					FruitItem fruit = iterator.next();					
					if(fruit.getNumber().equals(buyNumberText.getText())){
						if(fruit.getAmount()<Integer.parseInt(buyAmountText.getText())){
							JOptionPane.showMessageDialog(null, "现货没有这么多了。请重新输入");
							buyAmountText.setText("");
							buyAmountText.requestFocus();
						}
					}			
				}				
			}
		});
*/		
		//购买按钮监听
		buyBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String number = buyNumberText.getText();
				int amount = Integer.parseInt(buyAmountText.getText());
				buyPetItem(number,amount);//调用购买方法 				
			}
			
		});		
		//添加按钮监听
		addBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				addPetItem();  //调用添加方法
			}
		});			
		//修改按钮监听
		updateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				updatePetItem();  //调用修改方法
			}
		});
		//删除按钮监听
		delBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				delPetItem();   //调用删除方法
			}
		});
		//退出按钮监听
		exitBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);   
			}
		});
		
	}	
	
	//查询方法
	public void queryPetItem(){
		//定义表格头
		String[] thead = {"宠物编号","宠物名称","价格","数量","年龄"};
		//调用adminService的查询服务
		ArrayList<PetItem> dataList = adminService.queryPetItem();
		//将查询到的集合转为数组，方便为JTable赋值
		String[][] tbody = listToArray(dataList);
		//将查询到的结果为table赋值
		TableModel dataModel = new DefaultTableModel(tbody,thead);
		table.setModel(dataModel);		
	}
	//集合数组转为二维数组
	private String[][] listToArray(ArrayList<PetItem> list) {
		String[][] tbody = new String[list.size()][5];
		for(int i = 0; i < list.size();i++){
			PetItem pet = list.get(i);
			tbody[i][0] = pet.getNumber();
			tbody[i][1] = pet.getName();
			tbody[i][2] = pet.getPrice()+"";
			tbody[i][3] = pet.getAmount()+"";
			tbody[i][4] = pet.getAge()+"";
		}
		return tbody;
	}
	//添加方法
	public void addPetItem() {
		String addNumber = addNumberText.getText();
		String addName = addNameText.getText();
		String addPrice = addPriceText.getText();
		String addUnit = addUnitText.getText();
		String addAge = addAgeText.getText();
		boolean addSuccess = adminService.addPetItem(addNumber, addName, addPrice, addUnit,addAge);
		if(addSuccess){
			queryPetItem(); //添加后刷新表格
			addNumberText.setText("");
			addNameText.setText("");
			addPriceText.setText("");
			addUnitText.setText("");
			addAgeText.setText("");
		}else{
			//没有添加成功弹窗错误提示
			JOptionPane.showMessageDialog(this, "添加失败");
		}		
	}
	//修改方法
	public void updatePetItem() {
		String updateNumber = updateNumberText.getText();
		String updateName = updateNameText.getText();
		String updatePrice = updatePriceText.getText();
		String updateUnit = updateUnitText.getText();
		String updateAge = updateAgeText.getText();
		boolean updateSuccess = adminService.updatePetItem(updateNumber, updateName, updatePrice, updateUnit,updateAge);
		if(updateSuccess){
			queryPetItem();
			updateNumberText.setText("");
			updateNameText.setText("");
			updatePriceText.setText("");
			updateUnitText.setText("");
			updateAgeText.setText("");
		}else{
			JOptionPane.showMessageDialog(this, "修改失败，请检查数据");
		}		
	}
	//删除方法
	public void delPetItem() {
		String delNumber = delNumberText.getText();
		int delFlag = adminService.delPetItem(delNumber);
		if(delFlag == 1){
			queryPetItem();
			delNumberText.setText("");
		}else if(delFlag == 2){
			JOptionPane.showMessageDialog(this, "没有这个编号的宠物，请检查数据");
		}else {
			JOptionPane.showMessageDialog(this, "删除失败");
		}
	}
	//购买方法
	public void buyPetItem(String number,int amount) {
		PetItem pet = adminService.queryData(number);
		if(pet != null){
			if(pet.getAmount() < amount){
				JOptionPane.showMessageDialog(this, "宠物没有这么多了。请重新输入");
				buyAmountText.setText("");
				buyAmountText.requestFocus();			
		    }else{
		    	pet.setAmount(pet.getAmount() - amount);
		    	boolean updateSuccess = adminService.updatePetItem(pet.getNumber(),pet.getName(),pet.getPrice().toString(),String.valueOf(pet.getAmount()),pet.getPrice().toString());
				if(updateSuccess){
					queryPetItem();
					JOptionPane.showMessageDialog(this, "购买成功");
					buyNumberText.setText("");
					buyAmountText.setText("");
					Double money = pet.getPrice()*amount;
					SellPetItem sfruit = new SellPetItem(pet.getNumber(),pet.getName(),pet.getPrice(),amount,pet.getAge(),money);
					FileUtils.saveFruits(sfruit);  //将本次数据保存到本地文件
				}else{					
					JOptionPane.showMessageDialog(this, "购买失败");
				}
		    }			
		}else{
			JOptionPane.showMessageDialog(this, "购买失败");
		}
	}
}
