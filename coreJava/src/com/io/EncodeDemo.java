package com.io;

import java.io.UnsupportedEncodingException;

public class EncodeDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "权革";
		byte[] b1 = s.getBytes();
		for(byte b:b1){
			//把字节(转换成了int)以16进制的方式显示
			System.out.print(Integer.toHexString(b & 0xff)+" ");
		}
		
		
		System.out.println();
		try {
			byte[] bytes2 = s.getBytes("gbk");
			//gbk编码中文两个字节 英文一个字节
			for(byte b : bytes2){
				System.out.print(Integer.toHexString(b & 0xff)+" ");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println();
		try {
			byte[] bytes3 = s.getBytes("utf-8");
			//utf-8编码中文三个字节 英文一个字节
			for(byte b : bytes3){
				System.out.print(Integer.toHexString(b & 0xff)+" ");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println();
		try {
			byte[] bytes4 = s.getBytes("utf-16be");
			//utf-16be编码中文两个字节 英文两个字节
			for(byte b : bytes4){
				System.out.print(Integer.toHexString(b & 0xff)+" ");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*
		 * 当你的字节序列是某种编码时，这个时候想把字节序列变成字符串
		 * 也需要这种编码方式否则会出现乱码
		 */
		
	}
	
}
