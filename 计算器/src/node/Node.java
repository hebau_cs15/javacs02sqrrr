package node;

import java.util.regex.Pattern;

public class Node {
	private String data;
	private int flag;
	
	public void setData(String data) {
		this.data = data;
		//判断是数字还是运算符 如果是数字 则标为1如果是运算符则标为0
		if(Pattern.compile("[0-9]+").matcher(data).matches()){
			flag=1;
		}else{
			flag=0;
		}
	}
	public String getData() {
		return data;
	}
	public int getFlag() {
		return flag;
	}
	public Node() {
		
	}
	public Node(String data) {
		this.data = data ;
		//判断是数字还是运算符 如果是数字 则标为1如果是运算符则标为0
		if(Pattern.compile("[0-9]+").matcher(data).matches()){
			flag=1;
		}else{
			flag=0;
		}
	}
	
}
