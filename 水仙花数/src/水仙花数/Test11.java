package 水仙花数;
import java.math.*;  
import java.util.HashMap;
public class Test11  { 
 private int size = 0;//位数  
 private BigInteger MAX = null;  
 private BigInteger MIN = null;  
 HashMap<String, BigInteger> hm = null;//用来存放0`9的size次方
 public Test11(int length)//构造要求的水仙花的位数  
 {  
  this.size = length;  
  this.MAX = new BigInteger("10").pow(length).subtract(BigInteger.ONE);  
  this.MIN = new BigInteger("10").pow(length - 1);  
  this.hm = new HashMap<String, BigInteger>();  
  for (int i = 0; i <= 9; i++){  
   hm.put(i + "n", new BigInteger(i + "").pow(length));  
  }
 } 
 public static void main(String[] args)  {  
  long start = System.currentTimeMillis();  
  Test11 test = new Test11(21);  
  test.find();  
  long end = System.currentTimeMillis();  
  long time = end - start;  
  System.out.println("运行时间： " + time / 10e3 + "秒！");  
 }
 public void find()  {  
  BigInteger sum = BigInteger.ZERO;  
  int a0, a1, a2, a3, a4, a5, a6, a7, a8, a9;//分别是0到9数字出现的次数  
  for (a9 = 0; a9 <= this.size; a9++)  
  {  
   sum = sum.add(this.hm.get("9n").multiply(new BigInteger(a9 + "")));  
   if (sum.compareTo(MAX) > 0)  
    break;  
   for (a8 = 0; a8 <= this.size - a9; a8++)  
   {  
    sum = sum.add(this.hm.get("8n").multiply(  
      new BigInteger(a8 + "")));  
    if (sum.compareTo(MAX) > 0)  
     break;  
    for (a7 = 0; a7 <= this.size - a9 - a8; a7++)  
    {  
     sum = sum.add(this.hm.get("7n").multiply(  
       new BigInteger(a7 + "")));  
     if (sum.compareTo(MAX) > 0)  
      break;  
     for (a6 = 0; a6 <= this.size - a9 - a8 - a7; a6++)  
     {  
      sum = sum.add(this.hm.get("6n").multiply(  
        new BigInteger(a6 + "")));  
      if (sum.compareTo(MAX) > 0)  
       break;  
      for (a5 = 0; a5 <= this.size - a9 - a8 - a7 - a6; a5++)  
      {  
       sum = sum.add(this.hm.get("5n").multiply(  
         new BigInteger(a5 + "")));  
       if (sum.compareTo(MAX) > 0)  
        break;  
       for (a4 = 0; a4 <= this.size - a9 - a8 - a7 - a6  
         - a5; a4++)  
       {  
        sum = sum.add(this.hm.get("4n").multiply(  
          new BigInteger(a4 + "")));  
        if (sum.compareTo(MAX) > 0)  
         break;  
        for (a3 = 0; a3 <= this.size - a9 - a8 - a7  
          - a6 - a5 - a4; a3++)  
        {  
         sum = sum.add(this.hm.get("3n").multiply(  
           new BigInteger(a3 + "")));  
         if (sum.compareTo(MAX) > 0)  
          break;  
         for (a2 = 0; a2 <= this.size - a9 - a8 - a7  
           - a6 - a5 - a4 - a3; a2++)  
         {  
          sum = sum  
            .add(this.hm  
              .get("2n")  
              .multiply(  
                new BigInteger(  
                  a2 + "")));  
          if (sum.compareTo(MAX) > 0)  
           break;  
          for (a1 = 0; a1 <= this.size - a9 - a8  
            - a7 - a6 - a5 - a4 - a3 - a2; a1++)  
          {  
           sum = sum.add(this.hm.get("1n")  
             .multiply(  
               new BigInteger(a1  
                 + "")));  
           if (sum.compareTo(MAX) > 0)  
            break;           {  
            a0 = this.size - a9 - a8 - a7  
              - a6 - a5 - a4 - a3  
              - a2 - a1;  
            if (sum.compareTo(MAX) < 0  
              && sum.compareTo(MIN) > 0)  
             check(sum.toString(), a0,  
               a1, a2, a3, a4, a5,  
               a6, a7, a8, a9);  
           }  
           sum = sum.subtract(this.hm  
             .get("1n").multiply(  
               new BigInteger(a1  
                 + "")));  
          }  
          sum = sum  
            .subtract(this.hm  
              .get("2n")  
              .multiply(  
                new BigInteger(  
                  a2 + "")));  
         }  
         sum = sum.subtract(this.hm.get("3n")  
           .multiply(new BigInteger(a3 + "")));  
        }  
        sum = sum.subtract(this.hm.get("4n").multiply(  
          new BigInteger(a4 + "")));  
       }  
       sum = sum.subtract(this.hm.get("5n").multiply(  
         new BigInteger(a5 + "")));  
      }  
      sum = sum.subtract(this.hm.get("6n").multiply(  
        new BigInteger(a6 + "")));  
     }  
     sum = sum.subtract(this.hm.get("7n").multiply(  
       new BigInteger(a7 + "")));  
    }  
    sum = sum.subtract(this.hm.get("8n").multiply(  
      new BigInteger(a8 + "")));  
   }  
   sum = sum.subtract(this.hm.get("9n").multiply(  
     new BigInteger(a9 + "")));  
  } 
  } 
 public static void check(String str, int a0, int a1, int a2, int a3,  
   int a4, int a5, int a6, int a7, int a8, int a9)//判斷0到9出現的次數是否吻合sum結果中的  
 {  
  String temp = str;  
  if (a0 == count(temp, "0") && a1 == count(temp, "1")  
    && a2 == count(temp, "2") && a3 == count(temp, "3")  
    && a4 == count(temp, "4") && a5 == count(temp, "5")  
    && a6 == count(temp, "6") && a7 == count(temp, "7")  
    && a8 == count(temp, "8") && a9 == count(temp, "9"))  
   System.out.println(str);} public static int count(String str1, String str2)//統計sum中0到9出現的次數  
 {  
  String temp1 = str1;  
  String temp2 = str2;  
  int k = -1, count = 0;  
  while (true)  
  {  
   k = temp1.indexOf(temp2, k + 1);  
   if (k != -1)  
    count++;  
   else  
    break;  
  }  
  return count;  
 }
 }  
